public class Main {
    public static void main(String[] args) {

        SamsungPhone s_100 = new SamsungPhone("Samsung","S_100",256,"0.8 цг","Make front photograph");
        NokiaPhone _33_100 = new NokiaPhone("Nokia","_33_100",4,"0.1 гц");

        s_100.tostring();
        _33_100.tostring();
        s_100.makePhotographs();
        s_100.calls();
        _33_100.messages();
    }
}
