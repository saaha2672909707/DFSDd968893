import org.jetbrains.annotations.Contract;


import javax.xml.namespace.QName;

public class NokiaPhone extends Phone implements PhoneConnection{

    public NokiaPhone(String name, String model, int capacityMemory, String processorPower) {
        super(name, model, capacityMemory, processorPower);
    }

    public void tostring(){
        System.out.println("Name: [" + getName() + "] Model: [" + getModel() + "] Memory: [" + getCapacityMemory() +
                "] Power: [" + getProcessorPower() + "]");
    }


    @Override
    public void calls() {
        System.out.println("Make photograph");
    }

    @Override
    public void messages() {
        System.out.println("Messages available");
    }



}
