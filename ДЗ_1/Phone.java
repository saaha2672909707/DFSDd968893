public abstract class Phone {

    private   String name;
    private   String model;
    private   int capacityMemory;
    private   String  processorPower;

    public Phone(String name,String model,int capacityMemory,String processorPower){
        this.name = name;
        this.model = model;
        this.capacityMemory = capacityMemory;
        this.processorPower = processorPower;
    }


    public String getName(){
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModel(){
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getCapacityMemory() {
        return capacityMemory;
    }

    public void setCapacityMemory (int capacityMemory ){
        this.capacityMemory = capacityMemory;
    }

    public String getProcessorPower() {
        return processorPower;
    }

    public void setProcessorPower(String processorPower) {
        this.processorPower = processorPower;
    }
}
