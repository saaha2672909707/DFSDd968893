public interface PhoneMedia {

    void makePhotographs();
    void makeVideos();
}
