public class SamsungPhone extends Phone implements PhoneMedia, PhoneConnection {

    private String frontCamera;

    public String getFrontCamera(){
        return frontCamera;
    }

    public void setFrontCamera(String frontCamera) {
        this.frontCamera = frontCamera;
    }

    public SamsungPhone(String name, String model, int capacityMemory, String processorPower, String frontCamera) {
        super(name, model, capacityMemory, processorPower);
        this.frontCamera = frontCamera;
    }

    public void tostring(){
        System.out.println("Name: [" + getName() + "] Model: [" + getModel() + "] Memory: [" + getCapacityMemory() +
                "] Power: [" + getProcessorPower() + "] Front camera: [" + getFrontCamera() + "]");
    }




    @Override
    public void calls() {
        System.out.println("Calls available");
    }

    @Override
    public void messages() {
        System.out.println("Messages available");
    }

    @Override
    public void makePhotographs() {
        System.out.println("Make photograph");
    }

    @Override
    public void makeVideos() {
        System.out.println("Make video");
    }


}
