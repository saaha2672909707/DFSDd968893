import com.company.details.Engine;
import com.company.professions.Driver;
import com.company.vehicles.Car;
import com.company.vehicles.Lorry;
import com.company.vehicles.SportCar;

public class Main {
    public static void main(String[] args) {
        Driver bob = new Driver("Bob",35,"men",769,15);
        Car volkswagenCar = new Car("Volkswagen","Sedan",1700);
        Engine volkswagen = new Engine(250,"Volkswagen");
        Lorry dafCar = new Lorry("Daf","Truck",400,9000);
        Engine daf = new Engine(400,"Dar");
        SportCar bmwCar = new SportCar("Bmw","Sport",2000,350);
        Engine bmw = new Engine(750,"Bmw");
        bob.tostring();
        volkswagenCar.tostring();
        volkswagen.tostring();
        dafCar.tostring();
        daf.tostring();
        bmwCar.tostring();
        bmw.tostring();
        bmwCar.turnRight();
        dafCar.stop();
        volkswagenCar.start();







    }
}
