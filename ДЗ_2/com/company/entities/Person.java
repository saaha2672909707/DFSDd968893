package com.company.entities;

import com.company.professions.Driver;
public class Person  {


    private String name ;
    private int age ;
    private String sex;
    private int phoneNumber;



    public Person(String name, int age, String sex,int phoneNumber) {
        this.name = name;
        this.age = age;
        this.sex = sex;
        this.phoneNumber = phoneNumber;
    }


    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public int getAge (){
        return age;
    }

    public void setAge(int age){
        this.age = age;
    }

    public String getSex(){
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
