package com.company.professions;

import com.company.entities.Person;

public class Driver extends Person {
    private int drivingExperience;

    public Driver(String name, int age, String sex, int phoneNumber , int drivingExperience) {
        super(name, age, sex, phoneNumber);
        this.drivingExperience = drivingExperience;
    }

    public void tostring(){
        System.out.println("Name: [" + getName() + "] Age: [" + getAge() + "] Sex: [" + getSex() +
                "] Phone number: [" + getPhoneNumber() + "] Experience: [" + drivingExperience + "]") ;
    }



    public int getDrivingExperience() {
        return drivingExperience;
    }

    public void setDrivingExperience(int drivingExperience){
        this.drivingExperience = drivingExperience;
    }


}
