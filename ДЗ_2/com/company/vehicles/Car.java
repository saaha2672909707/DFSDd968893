package com.company.vehicles;

import com.company.details.Engine;
import com.company.professions.Driver;

 public class Car   {

     private String modelCar;
     private String classCar;
     private int weight;
     public  Car (String modelCar, String classCar, int weight){
         this.modelCar = modelCar;
         this.classCar = classCar;
         this.weight = weight;
     }

     public String getModelCar(){
         return modelCar;
     }

     public void setModelCar(String modelCar){
         this.modelCar = modelCar;
     }

     public String getClassCar (){
         return classCar;
     }

     public void setClassCar(String classCar){
         this.modelCar = modelCar;
     }

     public int getWeight() {
         return weight;
     }

     public void setWeight(int weight) {
         this.weight = weight;
     }


     public void tostring(){
         System.out.println( "Model: [" + getModelCar() + "] Class: [" + getClassCar() + "] Weight [" + getWeight() + "]");
     }

         public void start(){
             System.out.println("Go");
         }
         public void stop(){
             System.out.println("Stop");
         }
         public void turnRight(){
             System.out.println("Turn right");
         }
         public void turnLeft(){
             System.out.println("Turn left");
         }
}
