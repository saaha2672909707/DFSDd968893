package com.company.vehicles;

import com.company.details.Engine;
import com.company.entities.Person;


public class Lorry extends Car {

    private int fullWeight ;

    public Lorry(String modelCar, String classCar, int weight,int fullWeight) {
        super(modelCar, classCar, weight);
        this.fullWeight =fullWeight;
    }

    public int getFullWeight() {
        return fullWeight;
    }

    public void setFullWeight(int fullWeight) {
        this.fullWeight = fullWeight;
    }

    public void tostring() {
        System.out.println( "Model: [" + getModelCar() + "] Class: [" + getClassCar() + "] Weight [" + getWeight() +
                "] Full Weight: [" + getFullWeight() + "]");
    }

    @Override
    public void turnRight() {
        System.out.println("Lorry turn right");
    }

    @Override
    public void stop() {
        System.out.println("Lorry stop");
    }

    @Override
    public void start() {
        System.out.println("Lorry start");
    }

    @Override
    public void turnLeft() {
        System.out.println("Lorry turn left");
    }
}
