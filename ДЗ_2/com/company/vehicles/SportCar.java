package com.company.vehicles;

import com.company.details.Engine;
import com.company.entities.Person;
import com.company.professions.Driver;

public class SportCar extends Car{
    private int maxSpeed ;


    public SportCar(String modelCar, String classCar, int weight, int maxSpeed) {
        super(modelCar, classCar, weight);
        this.maxSpeed = maxSpeed;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public void tostring() {
        System.out.println( "Model: [" + getModelCar() + "] Class: [" + getClassCar() + "] Weight [" + getWeight() +
                "] Full Weight: [" + getModelCar() + "]");
    }

    @Override
    public void turnLeft() {
        System.out.println("Sport car turn left");
    }

    @Override
    public void start() {
        System.out.println("Sport car start");
    }

    @Override
    public void stop() {
        System.out.println("Sport car stop");
    }

    @Override
    public void turnRight() {
        System.out.println("Sport car turn right");
    }
}

